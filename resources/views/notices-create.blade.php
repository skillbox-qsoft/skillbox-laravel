@extends('layouts.app')

@section('content')

<h2 align='center' class="jumbotron-heading">Create News Article</h2>

@include('form.check')

<form action="{{ route('notices.store') }}" method="POST">
@csrf

@include('form.create')

<p align='center'>
<input type='submit' name='submit' style="width:50%;" value="Post Article">
</p>
</form>

@endsection
