<a href="{{ route('home') }}" class="navbar-brand d-flex align-items-center">Home</a>
<a href="{{ route('articles.index') }}" class="navbar-brand d-flex align-items-center">Posts</a>
<a href="{{ route('notices.index') }}" class="navbar-brand d-flex align-items-center">News</a>
<a href="{{ route('articles.create') }}" class="navbar-brand d-flex align-items-center">Compose</a>
<a href="{{ route('pushall.form') }}" class="navbar-brand d-flex align-items-center">Notify</a>
<a href="{{ route('contact') }}" class="navbar-brand d-flex align-items-center">Support</a>

@admin
<a href="{{ route('admin.sections.index') }}" class="navbar-brand d-flex align-items-center">Admin</a>
<a href="/telescope" class="navbar-brand d-flex align-items-center">Telescope</a>
@endadmin

<a href="{{ route('about') }}" class="navbar-brand d-flex align-items-center">About</a>
