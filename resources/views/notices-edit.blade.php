@extends('layouts.app')

@section('content')

<h2 align='center' class="jumbotron-heading">Update News Article</h2>

@include('form.check')

<form action="{{ route('notices.update', ['notice' => $notice->slug]) }}" method="POST">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}

@include('form.edit.notice')

<p align='center'>
<input type='submit' name='submit' style="width:25%;" value="Update Article">
</p>
</form>
<form action="{{ route('notices.destroy', ['notice' => $notice->slug]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
	<p align='center'>
		<input type='submit' style="width:25%;" value="Delete Article">
	</p>
</form>

@endsection
