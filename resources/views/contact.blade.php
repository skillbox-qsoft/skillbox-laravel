@extends('layouts.app')

@section('content')

<h2 align='center' class="jumbotron-heading">Contact Us</h2>
@if ($errors->all())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		    <p>{{ $error }}</p>
		@endforeach
	</div>
@endif
<form action="{{ route('send') }}" method="POST">
@csrf
<p align='center'>
<input type='text' name='email' style="width:50%;" placeholder="Your email address..." value="{{ old('email') }}">
</p>
<p align='center'>
<textarea name='message' style="width:50%;height:20%;" placeholder="What's on your mind?" rows=4>{{ old('message') }}</textarea>
</p>
<p align='center'>
<input type='hidden' name='received' style="width:50%;" placeholder="Current timestamp..." value="{{ date('Y-m-d H:i:s') }}">
</p>
<p align='center'>
<input type='submit' name='submit' style="width:50%;" value="Send Message">
</p>
</form>

@endsection