@extends('layouts.app')

@section('content')

  <div class="row">
    @foreach ($articles ?? '' as $article)
      <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
          <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" src="{{ $article->img }}" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">
              <b>{{ $article->title }}</b>
              @include('form.tags', ['tags' => $article->tags])
            </p>
            <p class="card-text">{{ $article->description }}</p>
          </div>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
              <a href="{{ route('articles.show', ['article' => $article->slug]) }}">View</a>
            </div>
            <div class="btn-group">
              <a href="{{ route('articles.edit', ['article' => $article->slug]) }}">Edit</a>
            </div>
            <div class="btn-group">
              <small class="text-muted"><p class="card-text">{{ $article->date }}</p></small>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    {{ $articles->links() }}
  </div>

@endsection