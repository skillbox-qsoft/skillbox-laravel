@extends('layouts.app')

@section('content')

    <h2 align='center' class="jumbotron-heading">{{ $notice->title }}</h2>
    <p align='center' class="lead text-muted">{{ $notice->date }}</p>
    @include('form.tags', ['tags' => $notice->tags])
    <p align='center' class="lead text-muted">
        <a href="{{ route('notices.index') }}">Go Back</a>
    </p>
    <p align='center' class="lead text-muted">
        <img width="85%" src="{{ $notice->img }}">
    </p>
    </section>
    <div class="container">
        <p class="lead text-muted">{{ $notice->content }}</p>
    </div>
    <div class="container">

        @include('form.comment.notice')

        @if (!empty($comments))

        @foreach ($comments as $comment)
            <p class="badge-secondary">{{ $comment->owner()->where('id', $comment->owner_id)->first()->name }} ({{ $comment->date }})</p>
            <p class="lead text-muted">{{ $comment->content }}</p>
        @endforeach

        {{ $comments->links() }}

        @endif
        
    </div>

@endsection