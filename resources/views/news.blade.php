@extends('layouts.app')

@section('content')

  <div class="row">
    @foreach ($notices ?? '' as $notice)
      <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
          <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" src="{{ $notice->img }}" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">
              <b>{{ $notice->title }}</b>
              @include('form.tags', ['tags' => $notice->tags])
            </p>
            <p class="card-text">{{ $notice->description }}</p>
          </div>
          <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
              <a href="{{ route('notices.show', ['notice' => $notice->slug]) }}">View</a>
            </div>
            <div class="btn-group">
              <a href="{{ route('notices.edit', ['notice' => $notice->slug]) }}">Edit</a>
            </div>
            <div class="btn-group">
              <small class="text-muted"><p class="card-text">{{ $notice->date }}</p></small>
            </div>
          </div>
        </div>
      </div>
    @endforeach
    
    {{ $notices->links() }}
  </div>

@endsection