@extends('layouts.app')

@section('content')

<h2 align='center' class="jumbotron-heading">Update Article</h2>

@include('form.check')

@can('update', $article)
<form action="{{ route('articles.update', ['article' => $article->slug]) }}" method="POST">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}

@include('form.edit.article')

<p align='center'>
	<input type='submit' name='submit' style="width:25%;" value="Update Article">
</p>
</form>
@endcan

<article-update article-id="{{ $article->id }}"></article-update>

<form action="{{ route('articles.destroy', ['article' => $article->slug]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
	<p align='center'>
		<input type='submit' style="width:25%;" value="Delete Article">
	</p>
</form>

@endsection
