@extends('layouts.app')

@section('content')

<h2 align='center' class="jumbotron-heading">Send Notification</h2>

@include('form.check')

<form action="{{ route('pushall.send') }}" method="POST">
@csrf

<p align='center'>
<input type='text' name='title' value="{{ old('title') }}" style="width:50%;" placeholder="Enter the notification title...">
</p>
<p align='center'>
<textarea name='text' style="width:50%;height:20%;" placeholder="What's on your mind?" rows=4>{{ old('text') }}</textarea>
</p>

<p align='center'>
<input type='submit' name='submit' style="width:50%;" value="Send">
</p>
</form>
</p>

@endsection
