@component('mail::message')
# Article Create Successful

The article was created: {{ $article->title }}

{{ $article->content }}

@component('mail::button', ['url' => '/articles/' . $article->slug])
Visit Article Page
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
