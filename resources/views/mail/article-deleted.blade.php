@component('mail::message')
# Article Delete Successful

The article was deleted: {{ $article->title }}

@component('mail::button', ['url' => '/articles'])
Go to Main Page
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
