@component('mail::message')
# Article Update Successful

The article was updated: {{ $article->title }}

{{ $article->content }}

@component('mail::button', ['url' => '/articles/' . $article->slug])
Visit Article Page
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
