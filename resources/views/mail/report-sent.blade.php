@component('mail::message')
# Report Sent Successful

The report has been sent:

{{ $string }}

@component('mail::button', ['url' => '/telescope/jobs'])
Go to Jobs Stats
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
