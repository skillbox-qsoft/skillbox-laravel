<p align='center'>
<input type='text' name='slug' value="{{ $notice->slug }}" style="width:50%;" placeholder="Post symbolic name...">
</p>
<p align='center'>
<input type='text' name='title' value="{{ $notice->title }}" style="width:50%;" placeholder="Add title here...">
</p>
<p align='center'>
<input type='text' name='tags' value="{{ old('tags', $notice->tags->pluck('name')->implode(',')) }}" style="width:50%;" placeholder="Add some tags...">
</p>
<p align='center'>
<input type='text' name='description' value="{{ $notice->description }}" style="width:50%;" placeholder="Add description here...">
</p>
<p align='center'>
<textarea name='content' style="width:50%;height:20%;" placeholder="What's on your mind?" rows=4>{{ $notice->content }}</textarea>
</p>
<p align='center'>
<input type='text' name='img' value="{{ $notice->img }}" style="width:50%;" placeholder="Add image here...">
</p>
