<p align='center'>
<input type='text' name='slug' value="{{ $article->slug }}" style="width:50%;" placeholder="Post symbolic name...">
</p>
<p align='center'>
<input type='text' name='title' value="{{ $article->title }}" style="width:50%;" placeholder="Add title here...">
</p>
<p align='center'>
<input type='text' name='tags' value="{{ old('tags', $article->tags->pluck('name')->implode(',')) }}" style="width:50%;" placeholder="Add some tags...">
</p>
<p align='center'>
<input type='text' name='description' value="{{ $article->description }}" style="width:50%;" placeholder="Add description here...">
</p>
<p align='center'>
<textarea name='content' style="width:50%;height:20%;" placeholder="What's on your mind?" rows=4>{{ $article->content }}</textarea>
</p>
<p align='center'>
<input type='text' name='img' value="{{ $article->img }}" style="width:50%;" placeholder="Add image here...">
</p>
