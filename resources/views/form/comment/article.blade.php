@include('form.check')

<form action="{{ route('articles.comment', ['article' => $article]) }}" method="GET">
    {{ method_field('POST') }}
    {{ csrf_field() }}

<p align='center'>
<textarea name='content' style="width:50%;height:20%;position:relative;" placeholder="What's on your mind?" rows=4>{{ old('content') }}</textarea>
<input type='submit' name='submit' style="width:50%;" value="Comment">
</p>

</form>