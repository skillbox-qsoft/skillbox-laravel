@php
  $tags = $tags ?? collect();
@endphp

@if (!empty($tags))
  <div>
    <p align='center'>
      @foreach ($tags as $tag)
        <a href="{{ route('tags', ['tag' => $tag->name]) }}" class='badge badge-secondary'>{{ $tag->name }}</a>
      @endforeach
    </p>
  </div>
@endif

