@php
    $tags = $tags ?? collect();
@endphp

@if ($tags->isNotEmpty())
  <div>
    <p align='center'>
      @foreach ($tags as $tag)
        <a href="{{ route('tag', ['tag' => $tag->name]) }}" class='badge badge-secondary'>{{ $tag->name }}</a>
      @endforeach
    </p>
    </div>
@endif
