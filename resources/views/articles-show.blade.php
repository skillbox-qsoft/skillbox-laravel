@extends('layouts.app')

@section('content')

    <h2 align='center' class="jumbotron-heading">{{ $article->title }}</h2>
    <p align='center' class="lead text-muted">{{ $article->date }}</p>
    @include('form.tags', ['tags' => $article->tags])
    <p align='center' class="lead text-muted">
        <a href="{{ route('articles.index') }}">Go Back</a>
    </p>
    <p align='center' class="lead text-muted">
        <img width="85%" src="{{ $article->img }}">
    </p>
    </section>
    <div class="container">
        <p class="lead text-muted">{{ $article->content }}</p>
    </div>
    <div class="container">

        @include('form.comment.article')

        @if (!empty($comments))

        @foreach ($comments as $comment)
            <p class="badge-secondary">{{ $comment->owner()->where('id', $comment->owner_id)->first()->name }} ({{ $comment->date }})</p>
            <p class="lead text-muted">{{ $comment->content }}</p>
        @endforeach

        {{ $comments->links() }}

        @endif
        
    </div>

@endsection