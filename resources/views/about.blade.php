@extends('layouts.app')

@section('content')

<p align='center' class="lead text-muted"><img style="width:18%;position:relative;" src='/favicon.png'></p>
<h2 align='center' class="jumbotron-heading">About Us</h2>
<p align='center' class="lead text-muted">This is a working Laravel website written on PHP and using MySQL database interface, with many additional components included in a framework extending website functionality. The project is intended only for educational purposes. You can improve its source code if you need.</p>

@endsection