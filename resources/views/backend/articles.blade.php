@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Article Index</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Symbolic Name</th>
<th style="width:40%;">Title</th>
<th style="width:10%;">Date</th>
<th style="width:8%;">Owner</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
{{ $articles->links() }}
@foreach ($articles as $article)
<tr>
<td>{{ $article->slug }}</td>
<td>{{ $article->title }}</td>
<td>{{ $article->date }}</td>
<td>{{ $article->owner()->where('id', $article->owner_id)->first()->name }}</td>
<form action="{{ route('admin.articles.destroy', ['article' => $article->slug]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
