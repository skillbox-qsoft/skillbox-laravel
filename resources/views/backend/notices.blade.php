@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Notices Index</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Symbolic Name</th>
<th style="width:40%;">Title</th>
<th style="width:10%;">Date</th>
<th style="width:8%;">Owner</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
{{ $notices->links() }}
@foreach ($notices as $notice)
<tr>
<td>{{ $notice->slug }}</td>
<td>{{ $notice->title }}</td>
<td>{{ $notice->date }}</td>
<td>{{ $notice->owner()->where('id', $notice->owner_id)->first()->name }}</td>
<form action="{{ route('admin.notices.destroy', ['notice' => $notice->slug]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
