@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">User Comments</h2>
<table style="width:95%;">
<thead>
<th style="width:40%;">Content</th>
<th style="width:10%;">Date</th>
<th style="width:8%;">Owner</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
{{ $comments->links() }}
@foreach ($comments as $comment)
<tr>
<td>{{ $comment->content }}</td>
<td>{{ $comment->date }}</td>
<td>{{ $comment->owner()->where('id', $comment->owner_id)->first()->name }}</td>
<form action="{{ route('admin.comments.destroy', ['comment' => $comment->id]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
