@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Admin Dashboard</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Section</th>
<th style="width:40%;">Description</th>
<th style="width:10%;">Link</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
@foreach ($sections as $section)
<tr>
<td>{{ $section->name }}</td>
<td>{{ $section->description }}</td>
<td><a href="{{ route($section->uri, []) }}">{{ $section->uri }}</a></td>
<form action="{{ route('admin.sections.destroy', ['section' => $section->name]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
