@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Changes Log</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Author</th>
<th style="width:10%;">Date</th>
<th style="width:30%;">Article</th>
<th style="width:40%;">Fields</th>
</thead>
<tbody>
@foreach ($changes as $change)
<tr>
<td>{{ $change->author()->where('id', $change->author_id)->first()->name }}</td>
<td>{{ $change->date }}</td>
<td>{{ $change->article()->where('id', $change->article_id)->first()->title }}</td>
<td>{{ $change->fields }}</td>
<form action="{{ route('admin.changes.destroy', ['change' => $change->id]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
