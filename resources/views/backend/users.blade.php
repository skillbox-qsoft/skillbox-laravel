@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">User Accounts</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Username</th>
<th style="width:40%;">Email</th>
<th style="width:10%;">Role</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
@foreach ($users as $user)
<tr>
<td>{{ $user->name }}</td>
<td>{{ $user->email }}</td>
<td>{{ ucfirst($user->role()->where('id', $user->role_id)->first()->name) }}</td>
<form action="{{ route('admin.users.destroy', ['user' => $user->email]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
