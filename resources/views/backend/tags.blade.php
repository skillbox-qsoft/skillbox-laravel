@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Article Tags</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">ID</th>
<th style="width:40%;">Name</th>
<th style="width:10%;">Actions</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
@foreach ($tags as $tag)
<tr>
<td>{{ $tag->id }}</td>
<td>{{ $tag->name }}</td>
<form action="{{ route('admin.tags.destroy', ['tag' => $tag->name]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
