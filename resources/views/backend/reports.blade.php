@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Website Reports</h2>
    
<form action="{{ route('backend.report') }}" method='POST'>
    {{ method_field('POST') }}
    {{ csrf_field() }}
<p align='center'>
<select id='reports' multiple='multiple' name='reports[]' style="width:30%;">
    <option value='articles'>Articles</option>
    <option value='changes'>Changes</option>
    <option value='comments'>Comments</option>
    <option value='mails'>Mail Queue</option>
    <option value='notices'>News Feed</option>
    <option value='roles'>Roles</option>
    <option value='tags'>Tags</option>
    <option value='users'>Users</option>
</select>
</p>
<p align='center'>
<input type='submit' name='submit' value='Generate Report'>
</p>
</form>

<p align='center'>{{ $result ?? '' }}</p>

@endadmin

@endsection
