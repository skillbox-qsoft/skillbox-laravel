@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">User Roles</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">ID</th>
<th style="width:40%;">Name</th>
<th style="width:40%;">Description</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
@foreach ($roles as $role)
<tr>
<td>{{ $role->id }}</td>
<td>{{ ucfirst($role->name) }}</td>
<td>{{ $role->description }}</td>
<form action="{{ route('admin.roles.destroy', ['role' => $role->name]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
