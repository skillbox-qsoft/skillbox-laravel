@extends('layouts.app')

@section('content')

@admin

<h2 align='center' class="jumbotron-heading">Mailing List</h2>
<table style="width:95%;">
<thead>
<th style="width:20%;">Email</th>
<th style="width:40%;">Message</th>
<th style="width:10%;">Received</th>
<th style="width:8%;">Actions</th>
</thead>
<tbody>
@foreach ($mails as $mail)
<tr>
<td>{{ $mail->email }}</td>
<td>{{ $mail->message }}</td>
<td>{{ $mail->received }}</td>
<form action="{{ route('admin.mails.destroy', ['mail' => $mail->email]) }}" method="POST">
	{{ method_field('DELETE') }}
	{{ csrf_field() }}
    <td><input type='submit' value="X"></td>
</form>
</tr>
@endforeach
</tbody>
</table>

@endadmin

@endsection
