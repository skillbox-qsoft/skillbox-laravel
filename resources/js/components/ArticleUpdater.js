export default {
    props: ['slug'],
    data() {
        return { hasUpdate: false }
    },
    mounted() {
        Echo
        .private('article.' + this.slug)
        .listen('ArticleUpdated', (data) => {
            this.hasUpdate = true;
        })
    },
    methods: {
        reload() {
            window.location.reload();
        }
    }
};
