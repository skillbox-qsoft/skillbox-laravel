import React from 'react';
import ReactDOM from 'react-dom';

function ArticleUpdate() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div v-if="hasUpdate">
                            <div className="card-header">Article update successful</div>
                            <div className="card-body">Article has been updated. <button className="card-button" onclick="reload()" class="btn btn-danger">Refresh Page</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ArticleUpdate;

if (document.getElementById('article-update')) {
    ReactDOM.render(<ArticleUpdate />, document.getElementById('article-update'));
}
