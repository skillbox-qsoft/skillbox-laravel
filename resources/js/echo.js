import Echo from "laravel-echo";

Echo
    .channel('hello')
    .listen('.event.happens', (e) => {
        alert(e.what);
    });

