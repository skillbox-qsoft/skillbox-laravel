<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\Article;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
     
    protected $model = Article::class;
    
    public function definition()
    {        
        $variants =
        [
            'boutiques', 'decor', 'horwich', 'fresh', 'romantic', 'paris', 'plan', 'cafe', 'carousel', 'tiffany', 'sabrina', 'norway', 'nature', 'ride', 'spa', 'tiramisu', 'quarter', 'retro', 'houses', 'coffee', 'city', 'alps'
        ];

        return [
            'slug' => $this->faker->unique()->slug(),
            'title' => $this->faker->unique()->sentence(),
            'description' => $this->faker->sentence(),
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'img' => 'https://github.com/eurohouse/eurocold/blob/main/back.'.$variants[rand(0, count($variants) - 1)].'.png?raw=true',
            'date' => Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString()),
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString()),
            'updated_at' => Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString())
        ];
    }
}
