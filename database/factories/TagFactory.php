<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\Tag;

class TagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
     
    protected $model = Tag::class;
    
    public function definition()
    {
        return [
            'name' => ucfirst($this->faker->unique()->words(1, true))
        ];
    }
}
