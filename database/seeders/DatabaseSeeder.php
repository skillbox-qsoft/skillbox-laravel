<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    public function run()
    {
        for ($i = 0; $i < 2; $i++) {
            $this->call(UsersTableSeeder::class);
            $this->call(ArticleSeeder::class);
        }
        $this->call(TagSeeder::class);
        $this->call(NoticeSeeder::class);
        $this->call(NewsTagSeeder::class);
    }
}
