<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Notice;
use App\Models\User;

class NoticeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $first = User::orderBy('id', 'ASC')->first();

        $notice = Notice::factory()->count(10)->create(['owner_id' => $first]);
    }
}
