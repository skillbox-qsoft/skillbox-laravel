<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Article;
use App\Models\User;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $next = User::where('id', '>', 1)->orderBy('id', 'DESC')->first();

        $article = Article::factory()->count(10)->create(['owner_id' => $next]);
    }
}
