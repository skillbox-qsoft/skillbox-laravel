<?php

namespace Database\Seeders;

use PDOException;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Article;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = Tag::factory()->count(5)->make();

        foreach ($tags as $tag) {
            try {
                $tags = Tag::factory()->create();
            } catch (\PDOException $e) {
                $tags = Tag::factory()->make();
                continue;
            }
        }

        $modelsCount = Article::count();
        $tagsCount = Tag::count();

        $startModels = $modelsCount - 20;
        $startTags = 0;

        $modelIds = Article::pluck('id')->all();
        $tagIds = Tag::pluck('id')->all();

        for ($i = $startModels; $i < $modelsCount; $i++) {
            $modelNo = $i + 1;
            for ($j = $startTags; $j < $tagsCount; $j++) {
                $tagNo = $j + 1;
                try {
                    DB::table('taggables')->insert([
                        'tag_id' => $tagNo,
                        'taggable_id' => $modelNo,
                        'taggable_type' => 'App\Models\Article'
                    ]);
                } catch (\PDOException $e) {
                    continue;
                }
            }
        }
    }
}
