<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Section;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('description');
            $table->string('uri')->unique();
            $table->timestamps();
        });

        Section::create([
            'name' => 'article',
            'description' => 'Articles',
            'uri' => 'admin.articles.index'
        ]);
        Section::create([
            'name' => 'change',
            'description' => 'Change History',
            'uri' => 'admin.changes.index'
        ]);
        Section::create([
            'name' => 'comment',
            'description' => 'Comments',
            'uri' => 'admin.comments.index'
        ]);
        Section::create([
            'name' => 'mail',
            'description' => 'Mail Queue',
            'uri' => 'admin.mails.index'
        ]);
        Section::create([
            'name' => 'notice',
            'description' => 'Notices',
            'uri' => 'admin.notices.index'
        ]);
        Section::create([
            'name' => 'report',
            'description' => 'Reports',
            'uri' => 'backend.reports'
        ]);
        Section::create([
            'name' => 'role',
            'description' => 'Roles',
            'uri' => 'admin.roles.index'
        ]);
        Section::create([
            'name' => 'tag',
            'description' => 'Tags',
            'uri' => 'admin.tags.index'
        ]);
        Section::create([
            'name' => 'user',
            'description' => 'Users',
            'uri' => 'admin.users.index'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
