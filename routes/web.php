<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ConfirmPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\PushServiceController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\Backend\AdminArticleController;
use App\Http\Controllers\Backend\AdminChangeController;
use App\Http\Controllers\Backend\AdminCommentController;
use App\Http\Controllers\Backend\AdminMailController;
use App\Http\Controllers\Backend\AdminNoticeController;
use App\Http\Controllers\Backend\AdminReportController;
use App\Http\Controllers\Backend\AdminRoleController;
use App\Http\Controllers\Backend\AdminSectionController;
use App\Http\Controllers\Backend\AdminTagController;
use App\Http\Controllers\Backend\AdminUserController;

Auth::routes();
Route::get('/websocket', function () {
    event(new \App\Events\SomethingHappens('WebSocket connection enabled'));
});
Route::get('/cache', function () {
    $data = Cache::putMany([
        'apple' => 'Apple, Inc.',
        'microsoft' => 'Microsoft Corporation',
    ], 3600);
    echo Cache::get('apple') . '<br>' . Cache::get('microsoft');
});
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/about', [HomeController::class, 'about'])->name('about');
Route::resource('/articles', ArticleController::class, ['names' => 'articles']);
Route::get('/tags/{tag}', [TagsController::class, 'index'])->name('tags');
Route::any('/articles/{article}/comment', [CommentController::class, 'addToArticle'])->name('articles.comment');
Route::resource('/notices', NoticeController::class, ['names' => 'notices']);
Route::any('/notices/{notice}/comment', [CommentController::class, 'addToNotice'])->name('notices.comment');
Route::get('/contact', [MailController::class, 'contact'])->name('contact');
Route::post('/mail/send', [MailController::class, 'send'])->name('send');
Route::get('/admin/reports/index', [AdminReportController::class, 'index'])->name('backend.reports');
Route::post('/admin/reports/send', [AdminReportController::class, 'send'])->name('backend.report');
Route::resource('/admin/articles', AdminArticleController::class, ['names' => 'admin.articles']);
Route::resource('/admin/changes', AdminChangeController::class, ['names' => 'admin.changes']);
Route::resource('/admin/comments', AdminCommentController::class, ['names' => 'admin.comments']);
Route::resource('/admin/mails', AdminMailController::class, ['names' => 'admin.mails']);
Route::resource('/admin/notices', AdminNoticeController::class, ['names' => 'admin.notices']);
Route::resource('/admin/roles', AdminRoleController::class, ['names' => 'admin.roles']);
Route::resource('/admin/sections', AdminSectionController::class, ['names' => 'admin.sections']);
Route::resource('/admin/tags', AdminTagController::class, ['names' => 'admin.tags']);
Route::resource('/admin/users', AdminUserController::class, ['names' => 'admin.users']);
Route::get('/service', [PushServiceController::class, 'form'])->name('pushall.form');
Route::post('/service', [PushServiceController::class, 'send'])->name('pushall.send');
