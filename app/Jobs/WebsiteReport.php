<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Mail;
use App\Mail\ReportSent;

class WebsiteReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $string = '';

        foreach ($this->data as $elem) {
            $ucfirst = ucfirst($elem);
            $tolower = strtolower($elem);
            $count = DB::table($tolower)->count();
            $string .= $ucfirst . ': ' . $count . '; ';
        }

        echo $string;

        if (config()->has('app.admin_mail')) {
            \Mail::to(config('app.admin_mail'))->send(
                new ReportSent($string)
            );
        }
    }
}
