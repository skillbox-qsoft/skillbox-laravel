<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

use App\Models\Tag;

class TagsController extends Controller
{
    public function index(Tag $tag)
    {
        return view('welcome', ['articles' => $tag->articles()->where('owner_id', auth()->id())->with('tags')->orderBy('slug', 'DESC')->paginate(15)]);
    }
}
