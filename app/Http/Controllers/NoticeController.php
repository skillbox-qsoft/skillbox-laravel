<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Models\Notice;
use App\Models\Tag;
use App\Models\Comment;
use App\Http\Requests\NoticeRequest;
use App\Service\TagSynchronizer;
use App\Service\ChangeLog;

class NoticeController extends Controller
{
    public function index()
    {
        $notices = \Cache::tags(['notices'])->remember('user_notices|' . auth()->id(), 3600, function () {
            return Notice::orderBy('date', 'DESC')->simplePaginate(9);
        });

        return view('news', ['notices' => $notices]);
    }

    public function create()
    {
        return view('notices-create', []);
    }

    public function show(Notice $notice)
    {
        $comments = $notice->comments()->where('owner_id', auth()->id())->orderBy('date', 'DESC')->simplePaginate(10);

        return view('news-show', ['notice' => $notice, 'comments' => $comments]);
    }

    public function store(NoticeRequest $request, TagSynchronizer $synchronizer, ChangeLog $changelog)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = auth()->id();

        $notice = Notice::create($validated);

        $synchronizer->sync($notice);
        $changelog->log($notice);

        return redirect()->route('notices.index');
    }

    public function edit(Notice $notice)
    {
        return view('notices-edit', ['notice' => $notice]);
    }

    public function update(NoticeRequest $request, Notice $notice, TagSynchronizer $synchronizer, ChangeLog $changelog)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = $notice->owner_id;

        $notice->update($validated);

        $synchronizer->sync($notice);
        $changelog->log($notice);

        return redirect()->route('notices.index');
    }

    public function destroy(Notice $notice)
    {
        $notice->delete();

        return redirect()->route('notices.index');
    }
}
