<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Article;
use App\Models\Notice;
use App\Models\Comment;
use App\Http\Requests\CommentRequest;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addToArticle(Article $article, CommentRequest $request)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = auth()->id();

        $article->comments()->create($validated);

        return redirect()->back();
    }

    public function addToNotice(Notice $notice, CommentRequest $request)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = auth()->id();

        $notice->comments()->create($validated);

        return redirect()->back();
    }
}
