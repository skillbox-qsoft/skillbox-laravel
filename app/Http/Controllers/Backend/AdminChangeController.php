<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Models\Change;

class AdminChangeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $changes = Change::orderBy('date', 'DESC')->get();

        return view('backend.changes', ['changes' => $changes]);
    }

    public function destroy(Change $change)
    {
        $change->delete();

        return redirect()->route('admin.changes.index');
    }
}
