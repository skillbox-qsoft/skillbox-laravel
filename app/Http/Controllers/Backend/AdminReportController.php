<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('backend.reports');
    }

    public function send(Request $request)
    {
        $data = $request->input('reports');

        \App\Jobs\WebsiteReport::dispatch($data);

        return view('backend.reports');
    }
}
