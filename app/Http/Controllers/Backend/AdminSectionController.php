<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Models\Section;

class AdminSectionController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(Section::class, 'section');
    }

    public function index()
    {
        $sections = Section::orderBy('name', 'ASC')->get();

        return view('backend.dashboard', ['sections' => $sections]);
    }

    public function destroy(Section $section)
    {
        $section->delete();

        return redirect()->route('admin.sections.index');
    }
}
