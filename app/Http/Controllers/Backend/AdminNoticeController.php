<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Models\Notice;

class AdminNoticeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */

    public function index()
    {
        $notices = Notice::orderBy('date', 'ASC')->simplePaginate(20);

        return view('backend.notices', ['notices' => $notices]);
    }

    public function destroy(Notice $notice)
    {
        $notice->delete();

        return redirect()->route('admin.notices.index');
    }
}
