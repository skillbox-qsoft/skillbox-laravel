<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Models\Mail;

class AdminMailController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(Mail::class, 'mail');
    }

    public function index()
    {
        $mails = Mail::orderBy('received', 'DESC')->get();

        return view('backend.mails', ['mails' => $mails]);
    }

    public function destroy(Mail $mail)
    {
        $mail->delete();

        return redirect()->route('admin.mails.index');
    }
}
