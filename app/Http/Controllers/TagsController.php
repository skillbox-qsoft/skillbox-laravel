<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tag;

class TagsController extends Controller
{
    public function index(Tag $tag)
    {
        return view('welcome', ['articles' => $tag->articles()->where('owner_id', auth()->id())->with('tags')->orderBy('date', 'DESC')->paginate(15)]);
    }
}
