<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Models\Article;
use App\Models\Mail;
use App\Models\Tag;
use App\Models\Comment;
use App\Models\Change;
use App\Mail\ArticleCreated;
use App\Mail\ArticleUpdated;
use App\Mail\ArticleDeleted;
use App\Http\Requests\ArticleRequest;
use App\Service\TagSynchronizer;
use App\Service\ChangeLog;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(Article::class, 'article');
    }

    public function index()
    {
        $articles = \Cache::tags(['articles'])->remember('user_articles|' . auth()->id(), 3600, function () {
            return Article::where('owner_id', auth()->id())->with('tags')->orderBy('date', 'DESC')->simplePaginate(9);
        });

        return view('welcome', ['articles' => $articles]);
    }
    
    public function create()
    {
        return view('articles-create', []);
    }

    public function show(Article $article)
    {
        $comments = $article->comments()->where('owner_id', auth()->id())->orderBy('date', 'DESC')->simplePaginate(10);

        return view('articles-show', ['article' => $article, 'comments' => $comments]);
    }
    
    public function store(ArticleRequest $request, TagSynchronizer $synchronizer, ChangeLog $changelog)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = auth()->id();

        $article = Article::create($validated);

        if (config()->has('app.admin_mail')) {
            \Mail::to(config('app.admin_mail'))->send(
                new ArticleCreated($article)
            );
        }

        $synchronizer->sync($article);
        $changelog->log($article);

        return redirect()->route('articles.index');
    }

    public function edit(Article $article)
    {
        return view('articles-edit', ['article' => $article]);
    }

    public function update(ArticleRequest $request, Article $article, TagSynchronizer $synchronizer, ChangeLog $changelog)
    {
        $validated = $request->validated();

        $validated['date'] = Carbon::now();
        $validated['owner_id'] = $article->owner_id;

        $article->update($validated);

        if (config()->has('app.admin_mail')) {
            \Mail::to(config('app.admin_mail'))->send(
                new ArticleUpdated($article)
            );
        }

        $synchronizer->sync($article);
        $changelog->log($article);

        return redirect()->route('articles.index');
    }

    public function destroy(Article $article)
    {
        $article->delete();

	    if (config()->has('app.admin_mail')) {
            \Mail::to(config('app.admin_mail'))->send(
                new ArticleDeleted($article)
            );
        }

        return redirect()->route('articles.index');
    }
}
