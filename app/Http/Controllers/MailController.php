<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Mail;

class MailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(Mail::class, 'mail');
    }

    public function contact()
    {
        return view('contact', []);
    }

    public function send(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|max:255',
            'message' => 'required|max:2048',
            'received' => 'required',
        ]);

        $mail = Mail::create($validated);

        return redirect()->route('contact');
    }

}
