<?php

namespace App\Service;

use Illuminate\Database\Eloquent\Model;
use App\Models\Change;

class ChangeLog
{
    public function log(Model $model)
    {
        $changes = array_keys($model->getChanges());
    
        $fields = implode(',', $changes);

        $currentChange =
        [
            'author_id' => $model->owner_id,
            'date' => $model->date,
            'article_id' => $model->id,
            'fields' => $fields,
        ];

        $change = Change::create($currentChange);
    }
}
