<?php

namespace App\Service;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;

class TagSynchronizer
{
    public function sync(Model $model)
    {
        $modelTags = $model->tags->keyBy('name');
    
        $tags = collect(explode(',', request('tags')))->keyBy(function ($item) { return $item; });
    
        $tagsToAttach = $tags->diffKeys($modelTags);
        $tagsToDetach = $modelTags->diffKeys($tags);
    
        foreach ($tagsToAttach as $tag) {
            $tag = Tag::firstOrCreate(['name' => $tag]);
            $model->tags()->attach($tag);
        }
    
        foreach ($tagsToDetach as $tag) {
            $tag->delete();
            $model->tags()->detach($tag);
        }
    }
}
