<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Comment extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'content',
        'article_id',
        'date',
        'owner_id'
    ];

    protected $sortable = [
        'id',
        'content',
        'article_id',
        'date',
        'owner_id'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function articles()
    {
        return $this->morphTo(Article::class, 'commentable');
    }

    public function notices()
    {
        return $this->morphTo(Notice::class, 'commentable');
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function about()
    {
        return $this->belongsTo(Article::class);
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['comments'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['comments'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['comments'])->flush();
        });
    }
}
