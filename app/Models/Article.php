<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Collections\ArticleCollection;
use App\Events\ArticleCreated;
use App\Events\ArticleUpdated;
use Illuminate\Support\Arr;
use Kyslik\ColumnSortable\Sortable;

class Article extends Model
{
    use HasFactory, Sortable, SoftDeletes;

    protected $dispatchableEvents = [
        'created' => ArticleCreated::class,
        'updated' => ArticleUpdated::class,
        'deleted' => ArticleUpdated::class
    ];

    protected $fillable = [
        'id',
        'slug',
        'title',
        'description',
        'content',
        'img',
        'date',
        'created_at',
        'updated_at',
        'deleted_at',
        'owner_id'
    ];

    protected $sortable = [
        'id',
        'slug',
        'title',
        'description',
        'content',
        'img',
        'date',
        'created_at',
        'updated_at',
        'deleted_at',
        'owner_id'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function collection(array $models = [])
    {
        return new ArticleCollection($models);
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['articles'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['articles'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['articles'])->flush();
        });
    }
}
