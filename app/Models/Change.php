<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Change extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'author_id',
        'date',
        'article_id',
        'fields'
    ];

    protected $sortable = [
        'id',
        'author_id',
        'date',
        'article_id',
        'fields'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['changes'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['changes'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['changes'])->flush();
        });
    }
}
