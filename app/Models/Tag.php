<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Tag extends Model
{
    use HasFactory;
    use Sortable;
    
    protected $fillable = [
        'id',
        'name'
    ];

    protected $sortable = [
        'id',
        'name'
    ];
    
    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }

    public function changes()
    {
        return $this->morphedByMany(Change::class, 'taggable');
    }

    public function notices()
    {
        return $this->morphedByMany(Notice::class, 'taggable');
    }

    public function users()
    {
        return $this->morphedByMany(User::class, 'taggable');
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['tags'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['tags'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['tags'])->flush();
        });
    }
}
