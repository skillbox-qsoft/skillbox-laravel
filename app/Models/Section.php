<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Section extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'name',
        'description',
        'uri'
    ];

    protected $sortable = [
        'id',
        'name',
        'description',
        'uri'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'name';
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['sections'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['sections'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['sections'])->flush();
        });
    }
}
