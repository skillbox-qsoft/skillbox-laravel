<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Mail extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'email',
        'message',
        'received'
    ];

    protected $sortable = [
        'id',
        'email',
        'message',
        'received'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'email';
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['mails'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['mails'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['mails'])->flush();
        });
    }
}
