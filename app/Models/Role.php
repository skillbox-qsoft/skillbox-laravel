<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Role extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    protected $sortable = [
        'id',
        'name',
        'description'
    ];
    
    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['roles'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['roles'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['roles'])->flush();
        });
    }
}
