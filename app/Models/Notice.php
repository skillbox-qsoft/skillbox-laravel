<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Collections\NoticeCollection;
use Kyslik\ColumnSortable\Sortable;

class Notice extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'id',
        'slug',
        'title',
        'description',
        'content',
        'img',
        'date',
        'owner_id'
    ];

    protected $sortable = [
        'id',
        'slug',
        'title',
        'description',
        'content',
        'img',
        'date',
        'owner_id'
    ];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function collection(array $models = [])
    {
        return new NoticeCollection($models);
    }

    protected static function booted()
    {
        static::created(function () {
            \Cache::tags(['notices'])->flush();
        });
        static::updated(function () {
            \Cache::tags(['notices'])->flush();
        });
        static::deleted(function () {
            \Cache::tags(['notices'])->flush();
        });
    }
}
