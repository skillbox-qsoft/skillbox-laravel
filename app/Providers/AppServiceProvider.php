<?php

namespace App\Providers;

use App\Models\Tag;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('layouts.sidebar', function ($view) {
            $view->with('tagsCloud', Tag::all());
        });

        Blade::component('components.alert', 'alert');

        Blade::if('admin', function() {
            return auth()->check() && auth()->user()->isAdmin();
        });

        /*
        \Queue::before(function (JobProcessing $event) {
            $event->connectionName;
            $event->job;
            $event->job->payload();
        });

        \Queue::after(function (JobProcessed $event) {
            $event->connectionName;
            $event->job;
            $event->job->payload();
        });

        \Queue::looping(function () {
            while (\DB::transactionLevel() > 0) {
                \DB::rollBack();
            }
        });
        */
    }

    public function register()
    {
        Collection::macro('toUpper', function () {
            return $this->map(function ($item) {
                return \Illuminate\Support\Str::upper($item);
            });
        });
    }
}
