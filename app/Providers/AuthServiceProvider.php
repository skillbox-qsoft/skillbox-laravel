<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Mail;
use App\Models\Role;
use App\Models\Section;
use App\Models\Tag;
use App\Models\User;

use App\Policies\ArticlePolicy;
use App\Policies\MailPolicy;
use App\Policies\RolePolicy;
use App\Policies\SectionPolicy;
use App\Policies\TagPolicy;
use App\Policies\UserPolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
        Mail::class => MailPolicy::class,
        Role::class => RolePolicy::class,
        Section::class => SectionPolicy::class,
        Tag::class => TagPolicy::class,
        User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
