<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Article;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportSent extends Mailable
{
    use Queueable, SerializesModels;

    public $string;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.report-sent');
    }
}
