<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Models\Article;

class ArticleChannel
{
    public function __construct()
    {
        //
    }

    public function join(User $user, Article $article)
    {
        return $user->id == $article->owner_id;
    }
}
