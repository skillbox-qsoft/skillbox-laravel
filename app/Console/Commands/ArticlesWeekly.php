<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Article;

class ArticlesWeekly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduling articles automatic mailing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subject = 'Scheduling article mail';
        $users = User::all();

        $end = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString())->format('Y-m-d');
        $start = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->subDays(7)->toDateTimeString())->format('Y-m-d');

        $articles = Article::whereDate('date', '>=', $start)->whereDate('date', '<=', $end)->orderBy('date', 'DESC')->get();

        $liner = $articles->pluck('title')->all();

        $values = implode(', ', $liner);

        $users->map->notify(new \App\Notifications\Articles($subject, $values));

        $this->info('Articles published from ' . $start . ' to ' . $end . ': ' . $values);
    }
}
