<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use App\Models\User;
use App\Models\Article;

class ArticlesForPeriod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:articles
    {from : First date in interval}
    {to : Latest date in interval}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List articles for certain period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subject = 'New articles found';
        $users = User::all();

        $start = $this->argument('from');
        $end = $this->argument('to');

        $articles = Article::whereDate('date', '>=', $start)->whereDate('date', '<=', $end)->orderBy('date', 'DESC')->get();

        $liner = $articles->pluck('title')->all();

        $values = implode(', ', $liner);

        $users->map->notify(new \App\Notifications\Articles($subject, $values));

        $this->info('Articles published from ' . $start . ' to ' . $end . ': ' . $values);
    }
}
