<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class ArticleCollection extends Collection
{
    public $articles = [];

    public function __construct($articles)
    {
        $this->articles = $articles;
    }

    public function index() {
        return $this->articles;
    }
}
