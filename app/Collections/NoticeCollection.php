<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class NoticeCollection extends Collection
{
    public $notices = [];

    public function __construct($notices)
    {
        $this->notices = $notices;
    }

    public function index() {
        return $this->notices;
    }
}
