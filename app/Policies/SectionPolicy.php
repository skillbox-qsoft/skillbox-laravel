<?php

namespace App\Policies;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

use App\Models\Section;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Section  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Section $model)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Section  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Section $model)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Section  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Section $model)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Section  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Section $model)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Section  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Section $model)
    {
        if ($user->isAdmin()) {
            return auth()->check() && auth()->user()->isAdmin();
        } else {
            return auth()->check();
        }
    }
}
