# My Laravel Project
My test project on Laravel. Using PHP >= 8, Laravel >= 8, MySQL >= 15.
Made with Composer on Arch Linux server host system.

# Study Progress
During the study period 8 of 10 total modules have been completed. The 10th one does not include homework part: it's an introdction module for the next part of my course. 9th one is on the go. Any help and support for my education process is appreciated. Thank you!
